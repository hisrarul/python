import json
import boto3
import logging

client = boto3.client('resourcegroupstaggingapi')
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event, context):
    arn = 'arn:aws:apigateway:us-east-1::/restapis/' + event['detail']['responseElements']['restapiUpdate']['restApiId']
    response = client.tag_resources(
    ResourceARNList=[
        arn
    ],
    Tags={
        'demo': 'test1'
        }
    )
    return ('Success!')