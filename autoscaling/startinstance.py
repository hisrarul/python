import boto3
import logging
import time
from botocore.exceptions import ClientError

logger = logging.getLogger()
logger.setLevel(logging.INFO)

ec2client = boto3.client('ec2')
asclient = boto3.client('autoscaling')

def startinstance(instanceId):
    ec2client.start_instances(
            InstanceIds=[
                instanceId
            ]
        )
    return ('Successfully start ' + instanceId)


def lambda_handler(event,context):
    instanceData = ec2client.describe_instances(
        Filters=[
            {
                'Name': 'tag:ApplicationRole',
                'Values': [
                    'jumphost',
                ]
            },
        ])
    for reservation in instanceData["Reservations"]:
        for instance in reservation["Instances"]:
            instanceId = instance["InstanceId"]
            logger.info('##AutoScaling Group')
            asData = asclient.describe_auto_scaling_instances(
            InstanceIds=[
                instanceId
                ]
            )
            logger.info(asData)
            logger.info('Starting instance, please wait.. ' + instanceId)
            # Get the ASG name
            try:
                try:
                    response = asclient.set_instance_health(
                            InstanceId=instanceId,
                            HealthStatus='Healthy',
                            ShouldRespectGracePeriod=False
                        )
                    startinstance(instanceId)
                except ClientError as e:
                    if 'IncorrectInstanceState' in str(e):
                        logger.info('Already Terminated ' + instanceId)
                asName = asData['AutoScalingInstances'][0]['AutoScalingGroupName']
                if asName:
                    # time.sleep(20)
                    response = asclient.resume_processes(
                            AutoScalingGroupName=asName,
                            ScalingProcesses=[
                                'Terminate'
                            ]
                        )
                    logger.info('Resume Process' + str(response))
            except:
                try:
                    startinstance(instanceId)
                except ClientError as e:
                    if 'IncorrectInstanceState' in str(e):
                        logger.info('Already Terminated ' + instanceId)
    return('Success!')
# lambda_handler()