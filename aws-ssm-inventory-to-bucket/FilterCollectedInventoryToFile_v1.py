import boto3
import json
import os

dict = {}

accountid = '255026805435'
region = 'us-east-1'
instanceId = 'i-055d319b0526a82fe'
file = instanceId + '.json'
bucketName = 'resource-data-sync-hyderabad'

s3 = boto3.resource('s3')

# Download the file from s3 bucket
path = 'AWS:InstanceInformation/accountid=' + accountid + '/region=' + region + '/resourcetype=ManagedInstanceInventory/'
object = path + file
s3.meta.client.download_file(bucketName, object, 'file.txt')

# Read specifc key value from json file and save as dictionary
with open('file.txt', 'rb') as json_file:
    data = json.load(json_file)
    # print(data)
for loop in range(1):
    dict.update({'PlatformName':data['PlatformName']})
    dict.update({'PlatformVersion':data['PlatformVersion']})
    dict.update({'ComputerName':data['ComputerName']})
    dict.update({'PlatformType':data['PlatformType']})
    # print(dict)
json_file.close()

# Download the file from s3 bucket
path = 'AWS:InstanceDetailedInformation/accountid=' + accountid + '/region=' + region + '/resourcetype=ManagedInstanceInventory/'
object = path + file
s3.meta.client.download_file(bucketName, object, 'file.txt')

# Read specifc key value from json file and save as dictionary
with open('file.txt', 'r') as json_file:
    data = json.load(json_file)
    # print(data)
for loop in range(1):
    dict.update({'CPUs':data['CPUs']})
    dict.update({'CPUCores':data['CPUCores']})
    dict.update({'CPUModel':data['CPUModel']})
    dict.update({'captureTime':data['captureTime']})
json_file.close()

# Download the file from s3 bucket
path = 'AWS:Network/accountid=' + accountid + '/region=' + region + '/resourcetype=ManagedInstanceInventory/'
object = path + file
s3.meta.client.download_file(bucketName, object, 'file.txt')

# Read specifc key value from json file and save as dictionary
with open('file.txt', 'r') as json_file:
    data = json.load(json_file)
    # print(data)
for loop in range(1):
    dict.update({'IPv4':data['IPV4']})
    dict.update({'SubnetMask':data['SubnetMask']})
    dict.update({'MacAddress':data['MacAddress']})
    dict.update({'DNSServer':data['DNSServer']})
    # print(json.dumps(dict))

with open('inventory.json', 'w') as inventory:
    json.dump(dict, inventory)


# Upload the file to s3 bucket
s3.Bucket(bucketName).upload_file('inventory.json', 'inventory.json')
