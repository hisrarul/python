import boto3
import json
import os

dict = {}
list = []

client = boto3.client('ssm')
ec2 = boto3.client('ec2')

paginator = client.get_paginator('describe_instance_information')
page_iterator = paginator.paginate()
instanceIds = []

for page in page_iterator:
    for result in range(0,len(page['InstanceInformationList'])):
        instanceIds.append(page['InstanceInformationList'][result]['InstanceId'])
        result += 1
    # print(page)
    # print(instanceIds)

accountid = '255026805435'
region = 'us-east-1'

for instanceId in instanceIds:

    InstanceData =   ec2.describe_instances(
        InstanceIds=[
        instanceId,
    ] )
    InstanceType = InstanceData['Reservations'][0]['Instances'][0]['InstanceType']
    AZ = InstanceData['Reservations'][0]['Instances'][0]['Placement']['AvailabilityZone']

    VolumeData = ec2.describe_volumes(
    Filters=[
        {
            'Name': 'attachment.instance-id',
            'Values': [
                instanceId,
                ],
            }
        ],
    )
    VolumeSize = VolumeData['Volumes'][0]['Size']

    file = instanceId + '.json'
    bucketName = 'resource-data-sync-hyderabad'

    s3 = boto3.resource('s3')

    # Download the file from s3 bucket
    path = 'AWS:InstanceInformation/accountid=' + accountid + '/region=' + region + '/resourcetype=ManagedInstanceInventory/'
    object = path + file
    s3.meta.client.download_file(bucketName, object, 'file.txt')

    # Read specifc key value from json file and save as dictionary
    with open('file.txt', 'rb') as json_file:
        data = json.load(json_file)
        # print(data)
    for loop in range(1):
        dict.update({'InstanceId':data['InstanceId']})
        dict.update({'PlatformName':data['PlatformName']})
        dict.update({'PlatformVersion':data['PlatformVersion']})
        dict.update({'ComputerName':data['ComputerName']})
        dict.update({'PlatformType':data['PlatformType']})
        dict.update({'InstanceType':InstanceType})
        dict.update({'Location':AZ})
        dict.update({'VolumeSize':VolumeSize})
        # print(dict)
    json_file.close()

    # Download the file from s3 bucket
    path = 'AWS:InstanceDetailedInformation/accountid=' + accountid + '/region=' + region + '/resourcetype=ManagedInstanceInventory/'
    object = path + file
    s3.meta.client.download_file(bucketName, object, 'file.txt')

    # Read specifc key value from json file and save as dictionary
    with open('file.txt', 'r') as json_file:
        data = json.load(json_file)
        # print(data)
    for loop in range(1):
        dict.update({'CPUs':data['CPUs']})
        dict.update({'CPUCores':data['CPUCores']})
        dict.update({'CPUModel':data['CPUModel']})
        dict.update({'captureTime':data['captureTime']})
        # print(dict)
    json_file.close()

    # Download the file from s3 bucket
    path = 'AWS:Network/accountid=' + accountid + '/region=' + region + '/resourcetype=ManagedInstanceInventory/'
    object = path + file
    s3.meta.client.download_file(bucketName, object, 'file.txt')

    # Read specifc key value from json file and save as dictionary
    with open('file.txt', 'r') as json_file:
        data = json.load(json_file)
        # print(data)
    for loop in range(1):
        dict.update({'IPv4':data['IPV4']})
        # dict.update({'SubnetMask':data['SubnetMask']})
        dict.update({'MacAddress':data['MacAddress']})
        # dict.update({'DNSServer':data['DNSServer']})

# Append the dictionary for each instance to the list     
    list.append(dict)
    with open('inventory.json', 'w') as inventory:
        json.dump(list, inventory)


    # Upload the file to s3 bucket
    s3.Bucket(bucketName).upload_file('inventory.json', 'inventory.json')
