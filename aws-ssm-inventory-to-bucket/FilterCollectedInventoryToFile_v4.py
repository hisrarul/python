import boto3
import json
import os
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

dict = {}
region = 'us-east-1'
instanceIds = []

client = boto3.client('ssm')
ec2 = boto3.client('ec2')

def instance_data(instanceId):
    response = ec2.describe_instances(
        InstanceIds=[
            instanceId,
    ] )
    return response


def lambda_handler(event,context):
    instanceData = ec2.describe_instances(
        Filters=[
            {
                'Name': 'tag:SSM',
                'Values': [
                    'inventory',
                ]
            },
        ])

# Create a list of all instances tagged with SSM and inventory
    for reservation in instanceData["Reservations"]:
        for instance in reservation["Instances"]:
            instanceId = instance["InstanceId"]
            instance_data(instanceId)
            instanceIds.append(instanceId)
    logger.info(instanceIds)

    for instanceId in instanceIds:
        logger.info(instanceId)
        InstanceData = instance_data(instanceId)
        InstanceType = InstanceData['Reservations'][0]['Instances'][0]['InstanceType']
        AZ = InstanceData['Reservations'][0]['Instances'][0]['Placement']['AvailabilityZone']
        accountId = InstanceData['Reservations'][0]['Instances'][0]['NetworkInterfaces'][0]['OwnerId']
        VolumeData = ec2.describe_volumes(
        Filters=[{
                'Name': 'attachment.instance-id',
                'Values': [
                    instanceId
                    ]} ])
        VolumeSize = VolumeData['Volumes'][0]['Size']
        file = instanceId + '.json'
        bucketName = 'resource-data-sync-hyderabad'
        s3 = boto3.resource('s3')
        # Download the file from s3 bucket
        path = 'AWS:InstanceInformation/accountid=' + accountId + '/region=' + region + '/resourcetype=ManagedInstanceInventory/'
        object = path + file
        s3.meta.client.download_file(bucketName, object, '/tmp/file.txt')
        # Read specifc key value from json file and save as dictionary
        with open('/tmp/file.txt', 'rb') as json_file:
            data = json.load(json_file)
            # print(data)
        for loop in range(1):
            dict.update({'InstanceId':data['InstanceId']})
            dict.update({'PlatformName':data['PlatformName']})
            dict.update({'PlatformVersion':data['PlatformVersion']})
            dict.update({'ComputerName':data['ComputerName']})
            dict.update({'PlatformType':data['PlatformType']})
            dict.update({'InstanceType':InstanceType})
            dict.update({'Location':AZ})
            dict.update({'VolumeSize':VolumeSize})
            # print(dict)
        json_file.close()
        # Download the file from s3 bucket
        path = 'AWS:InstanceDetailedInformation/accountid=' + accountId + '/region=' + region + '/resourcetype=ManagedInstanceInventory/'
        object = path + file
        s3.meta.client.download_file(bucketName, object, '/tmp/file.txt')
        # Read specifc key value from json file and save as dictionary
        with open('/tmp/file.txt', 'r') as json_file:
            data = json.load(json_file)
            # print(data)
        for loop in range(1):
            dict.update({'CPUs':data['CPUs']})
            dict.update({'CPUCores':data['CPUCores']})
            dict.update({'CPUModel':data['CPUModel']})
            dict.update({'captureTime':data['captureTime']})
            # print(dict)
        json_file.close()
        # Download the file from s3 bucket
        path = 'AWS:Network/accountid=' + accountId + '/region=' + region + '/resourcetype=ManagedInstanceInventory/'
        object = path + file
        s3.meta.client.download_file(bucketName, object, '/tmp/file.txt')
        # Read specifc key value from json file and save as dictionary
        with open('/tmp/file.txt', 'r') as json_file:
            data = json.load(json_file)
            # print(data)
        for loop in range(1):
            dict.update({'IPv4':data['IPV4']})
            dict.update({'MacAddress':data['MacAddress']})
#Append the inventory file        
        with open('/tmp/inventory.json', 'a+') as inventory:
            json.dump(dict, inventory)
#Upload the file to s3 bucket
        s3.Bucket(bucketName).upload_file('/tmp/inventory.json', 'inventory.json')
    return ('Success!')