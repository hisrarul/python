import boto3

ec2 = boto3.client('ec2', 'us-east-1')

def lambda_handler(event,context):
    instanceData = ec2.describe_instances(
        Filters=[
            {
                'Name': 'tag:CostCenter',
                'Values': [
                    '001234',
                ]
            },
        ],
    )
    for index in instanceData['Reservations']:
    # print(instanceData['Reservations'][1]['Instances'][0]['BlockDeviceMappings'][0]['Ebs']['VolumeId'])
        # ['Instances'][0]['BlockDeviceMappings'][0]['Ebs']['VolumeId'])
        volumeId = index['Instances'][0]['BlockDeviceMappings'][0]['Ebs']['VolumeId']
        
        snapshot = ec2.create_snapshot(
        VolumeId=volumeId,
        TagSpecifications=[
            {
                'ResourceType': 'snapshot',
                'Tags': [
                    {
                        'Key': 'CostCenter',
                        'Value': '001234'
                    },
                ]
            },
        ]
        )
        print(snapshot)