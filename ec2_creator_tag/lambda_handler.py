from __future__ import print_function
import boto3
import json
import logging
import time
import datetime

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def lambda_handler(event, context):
   logger.info(event)
   sts_client = boto3.client('sts')

   account_id = event['account']
   creator_arn = event['detail']['userIdentity']['arn']
   logger.info(account_id)

   assumed_role_object=sts_client.assume_role(
      # RoleArn="arn:aws:iam::817338342595:role/CrossAccountFromIsrar",
      RoleArn='arn:aws:iam::' + str(account_id) + ':role/CrossAccountFromIsrar',
      RoleSessionName='RoleSession1'
   )

   credentials=assumed_role_object['Credentials']


   ec2 = boto3.client('ec2',
      aws_access_key_id=credentials['AccessKeyId'],
      aws_secret_access_key=credentials['SecretAccessKey'],
      aws_session_token=credentials['SessionToken'],
   )

   ec2 = boto3.resource('ec2',
      aws_access_key_id=credentials['AccessKeyId'],
      aws_secret_access_key=credentials['SecretAccessKey'],
      aws_session_token=credentials['SessionToken'],
   )

   #{u'account': u'817338342595', u'region': u'us-east-1', u'detail': {u'eventVersion': u'1.05', u'eventID': u'a07b41e6-25a6-4be9-a6c0-95d5e4a3c434', u'eventTime': u'2019-07-05T04:43:31Z', u'requestParameters': {u'groupSet': {u'items': [{u'groupId': u'sg-058a18c790787940f'}]}, u'monitoring': {u'enabled': False}, u'capacityReservationSpecification': {u'capacityReservationPreference': u'open'}, u'instanceInitiatedShutdownBehavior': u'stop', u'tenancy': u'default', u'creditSpecification': {u'cpuCredits': u'standard'}, u'blockDeviceMapping': {u'items': [{u'deviceName': u'/dev/xvda', u'ebs': {u'deleteOnTermination': True, u'volumeSize': 8, u'volumeType': u'gp2'}}]}, u'instancesSet': {u'items': [{u'minCount': 1, u'maxCount': 1, u'keyName': u'autoscaling', u'imageId': u'ami-0b898040803850657'}]}, u'disableApiTermination': False, u'instanceType': u't2.micro', u'ebsOptimized': False}, u'eventType': u'AwsApiCall', u'responseElements': {u'ownerId': u'817338342595', u'groupSet': {}, u'reservationId': u'r-0ae076a70eb89c3c4', u'requestId': u'54ede52b-ff74-4dbe-a25d-8616b2d47fc3', u'instancesSet': {u'items': [{u'productCodes': {}, u'vpcId': u'vpc-04434f7f23c27a607', u'cpuOptions': {u'threadsPerCore': 1, u'coreCount': 1}, u'instanceId': u'i-051db8cb88f58ab0b', u'imageId': u'ami-0b898040803850657', u'keyName': u'autoscaling', u'subnetId': u'subnet-0b43102c95508605d', u'amiLaunchIndex': 0, u'instanceType': u't2.micro', u'groupSet': {u'items': [{u'groupName': u'launch-wizard-43', u'groupId': u'sg-058a18c790787940f'}]}, u'monitoring': {u'state': u'disabled'}, u'capacityReservationSpecification': {u'capacityReservationPreference': u'open'}, u'stateReason': {u'message': u'pending', u'code': u'pending'}, u'privateIpAddress': u'172.31.82.193', u'virtualizationType': u'hvm', u'privateDnsName': u'ip-172-31-82-193.ec2.internal', u'sourceDestCheck': True, u'blockDeviceMapping': {}, u'placement': {u'tenancy': u'default', u'availabilityZone': u'us-east-1c'}, u'hypervisor': u'xen', u'networkInterfaceSet': {u'items': [{u'status': u'in-use', u'macAddress': u'12:b2:cb:06:d5:44', u'sourceDestCheck': True, u'vpcId': u'vpc-04434f7f23c27a607', u'ipv6AddressesSet': {}, u'networkInterfaceId': u'eni-0857a973e4b381989', u'privateDnsName': u'ip-172-31-82-193.ec2.internal', u'groupSet': {u'items': [{u'groupName': u'launch-wizard-43', u'groupId': u'sg-058a18c790787940f'}]}, u'interfaceType': u'interface', u'attachment': {u'status': u'attaching', u'deviceIndex': 0, u'deleteOnTermination': True, u'attachmentId': u'eni-attach-0b3651adf9fe44ce1', u'attachTime': 1562301810000}, u'tagSet': {}, u'subnetId': u'subnet-0b43102c95508605d', u'ownerId': u'817338342595', u'privateIpAddressesSet': {u'item': [{u'privateDnsName': u'ip-172-31-82-193.ec2.internal', u'primary': True, u'privateIpAddress': u'172.31.82.193'}]}, u'privateIpAddress': u'172.31.82.193'}]}, u'ebsOptimized': False, u'launchTime': 1562301810000, u'architecture': u'x86_64', u'instanceState': {u'code': 0, u'name': u'pending'}, u'rootDeviceType': u'ebs', u'rootDeviceName': u'/dev/xvda'}]}}, u'awsRegion': u'us-east-1', u'eventName': u'RunInstances', u'userIdentity': {u'userName': u'mit', u'principalId': u'AIDA34TJJLTBQJ5XSKWFX', u'accessKeyId': u'ASIA34TJJLTBXAEZXOWB', u'invokedBy': u'signin.amazonaws.com', u'sessionContext': {u'attributes': {u'creationDate': u'2019-07-05T02:55:08Z', u'mfaAuthenticated': u'false'}}, u'type': u'IAMUser', u'arn': u'arn:aws:iam::817338342595:user/mit', u'accountId': u'817338342595'}, u'eventSource': u'ec2.amazonaws.com', u'requestID': u'54ede52b-ff74-4dbe-a25d-8616b2d47fc3', u'userAgent': u'signin.amazonaws.com', u'sourceIPAddress': u'103.88.217.233'}, u'detail-type': u'AWS API Call via CloudTrail', u'source': u'aws.ec2', u'version': u'0', u'time': u'2019-07-05T04:43:31Z', u'id': u'9f792d1b-9aa3-2bd1-4c67-8224abcb420f', u'resources': []}
   mytags = [{
      "Key" : "TagName", 
         "Value" : "TagValue"
      }, 
      {
         "Key" : "APP",
         "Value" : "webapp"
      },
      {
         "Key" : "Creator",
         "Value" : creator_arn
      },
      {
         "Key" : "Team", 
         "Value" : "xteam"
      }]

   instance_id = event['detail']['responseElements']['instancesSet']['items'][0]['instanceId']
   print(instance_id)
   response = ec2.create_tags(
               Resources = [instance_id],
               Tags= mytags
           )
   logger.info(response)
#{'version': '0', 'id': '0ab28079-b8ac-e3a4-7d0f-0da019a2186d', 'detail-type': 'AWS API Call via CloudTrail', 'source': 'aws.ec2', 'account': '847970100731', 'time': '2019-07-06T04:10:43Z', 'region': 'us-east-1', 'resources': [], 'detail': {'eventVersion': '1.05', 'userIdentity': {'type': 'Root', 'principalId': '847970100731', 'arn': 'arn:aws:iam::847970100731:root', 'accountId': '847970100731', 'accessKeyId': 'ASIA4K3XU4X56X22OEUM', 'userName': 'israrulhaque', 'sessionContext': {'attributes': {'mfaAuthenticated': 'false', 'creationDate': '2019-07-06T02:30:07Z'}}, 'invokedBy': 'signin.amazonaws.com'}, 'eventTime': '2019-07-06T04:10:43Z', 'eventSource': 'ec2.amazonaws.com', 'eventName': 'RunInstances', 'awsRegion': 'us-east-1', 'sourceIPAddress': '103.95.122.204', 'userAgent': 'signin.amazonaws.com', 'requestParameters': {'instancesSet': {'items': [{'imageId': 'ami-0b898040803850657', 'minCount': 1, 'maxCount': 1, 'keyName': 'haque65_virginia'}]}, 'instanceType': 't2.micro', 'blockDeviceMapping': {'items': [{'deviceName': '/dev/xvda', 'ebs': {'volumeSize': 8, 'deleteOnTermination': True, 'volumeType': 'gp2'}}]}, 'tenancy': 'default', 'monitoring': {'enabled': False}, 'disableApiTermination': False, 'instanceInitiatedShutdownBehavior': 'stop', 'networkInterfaceSet': {'items': [{'deviceIndex': 0, 'subnetId': 'subnet-0ace6737cde7868f6', 'description': 'Primary network interface', 'deleteOnTermination': True, 'groupSet': {'items': [{'groupId': 'sg-02e1063ded43bd8c0'}]}, 'ipv6AddressCount': 0}]}, 'ebsOptimized': False, 'creditSpecification': {'cpuCredits': 'standard'}, 'capacityReservationSpecification': {'capacityReservationPreference': 'open'}}, 'responseElements': {'requestId': 'ebaff56f-a3b1-4223-bc9e-4633684b5044', 'reservationId': 'r-0de7df58413bc979a', 'ownerId': '847970100731', 'groupSet': {}, 'instancesSet': {'items': [{'instanceId': 'i-09349f9c475d3d48f', 'imageId': 'ami-0b898040803850657', 'instanceState': {'code': 0, 'name': 'pending'}, 'privateDnsName': 'ip-10-0-0-223.ec2.internal', 'keyName': 'haque65_virginia', 'amiLaunchIndex': 0, 'productCodes': {}, 'instanceType': 't2.micro', 'launchTime': 1562386243000, 'placement': {'availabilityZone': 'us-east-1a', 'tenancy': 'default'}, 'monitoring': {'state': 'disabled'}, 'subnetId': 'subnet-0ace6737cde7868f6', 'vpcId': 'vpc-062d5c1887fb1a293', 'privateIpAddress': '10.0.0.223', 'stateReason': {'code': 'pending', 'message': 'pending'}, 'architecture': 'x86_64', 'rootDeviceType': 'ebs', 'rootDeviceName': '/dev/xvda', 'blockDeviceMapping': {}, 'virtualizationType': 'hvm', 'hypervisor': 'xen', 'groupSet': {'items': [{'groupId': 'sg-02e1063ded43bd8c0', 'groupName': 'launch-wizard-3'}]}, 'sourceDestCheck': True, 'networkInterfaceSet': {'items': [{'networkInterfaceId': 'eni-03c78d7ca8e5d933d', 'subnetId': 'subnet-0ace6737cde7868f6', 'vpcId': 'vpc-062d5c1887fb1a293', 'description': 'Primary network interface', 'ownerId': '847970100731', 'status': 'in-use', 'macAddress': '02:f4:4b:18:5a:2a', 'privateIpAddress': '10.0.0.223', 'privateDnsName': 'ip-10-0-0-223.ec2.internal', 'sourceDestCheck': True, 'interfaceType': 'interface', 'groupSet': {'items': [{'groupId': 'sg-02e1063ded43bd8c0', 'groupName': 'launch-wizard-3'}]}, 'attachment': {'attachmentId': 'eni-attach-0b7d8eb2defc0d3a8', 'deviceIndex': 0, 'status': 'attaching', 'attachTime': 1562386243000, 'deleteOnTermination': True}, 'privateIpAddressesSet': {'item': [{'privateIpAddress': '10.0.0.223', 'privateDnsName': 'ip-10-0-0-223.ec2.internal', 'primary': True}]}, 'ipv6AddressesSet': {}, 'tagSet': {}}]}, 'ebsOptimized': False, 'cpuOptions': {'coreCount': 1, 'threadsPerCore': 1}, 'capacityReservationSpecification': {'capacityReservationPreference': 'open'}}]}}, 'requestID': 'ebaff56f-a3b1-4223-bc9e-4633684b5044', 'eventID': 'ff2608a5-0072-4b59-add1-779bf576dd49', 'eventType': 'AwsApiCall'}}
