import json
import logging
import boto3

logger = logging.getLogger()
logger.setLevel(logging.INFO)

ec2 = boto3.resource('ec2')

def lambda_handler(event, context):
    logger.info('##Event')
    logger.info(event)
    username = event['detail']['userIdentity']['userName']
    logger.info('##UserName')
    logger.info(username)
    instance_id = event['detail']['responseElements']['instancesSet']['items'][0]['instanceId']
    logger.info('##InstanceId: ' + instance_id)
    ec2.create_tags(
                       Resources = [instance_id],
                       Tags= [
                                {
                                    'Key': 'Owner',
                                    'Value': username
                                },
                            ]
                   )
    logger.info('Success!')