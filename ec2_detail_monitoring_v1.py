import boto3
import sys
from botocore.exceptions import ClientError

ec2 = boto3.client('ec2')
# print(sys.argv[0])
# print(sys.argv[1])

if sys.argv[1] == 'ON':
    try:
        response = ec2.monitor_instances(InstanceIds=['i-0e29e1b1f5398123e'], DryRun=True)
    except ClientError:
        response = ec2.monitor_instances(InstanceIds=['i-0e29e1b1f5398123e'], DryRun=False)
else:
    try:
        response = ec2.unmonitor_instances(InstanceIds=['i-0e29e1b1f5398123e'], DryRun=True)
    except ClientError:
        response = ec2.unmonitor_instances(InstanceIds=['i-0e29e1b1f5398123e'], DryRun=False)

print('Current State of Detailed Monitoring is ' + response['InstanceMonitorings'][0]['Monitoring']['State'] + '.')