import boto3
import sys
from botocore.exceptions import ClientError

ec2 = boto3.client('ec2')
instances = ec2.describe_instances()

if sys.argv[1] == 'ON':
    monitored_instances = []
    for instance in instances['Reservations']:
        instance_id = instance['Instances'][0]['InstanceId']
        monitored_instances.append(instance_id)
        try:
            response = ec2.monitor_instances(InstanceIds=[instance_id], DryRun=True)
        except ClientError:
            response = ec2.monitor_instances(InstanceIds=[instance_id], DryRun=False)
    print('Detailed Monitoring has been enabled on following instances: \n' + str(monitored_instances))
else:
    unmonitored_instances = []
    for instance in instances['Reservations']:
        instance_id = instance['Instances'][0]['InstanceId']
        unmonitored_instances.append(instance_id)
        try:
            response = ec2.unmonitor_instances(InstanceIds=[instance_id], DryRun=True)
        except ClientError:
            response = ec2.unmonitor_instances(InstanceIds=[instance_id], DryRun=False)

    print('Detailed Monitoring has been disabled on following instances: \n' + str(unmonitored_instances))