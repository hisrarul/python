import boto3

ec2 = boto3.client('ec2')
response = ec2.describe_instances()

print('List of instance in your account: \n')
for item in response['Reservations']:
    print(item['Instances'][0]['InstanceId'])