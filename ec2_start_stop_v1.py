import sys
import boto3
from botocore.exceptions import ClientError

try:
    action = sys.argv[1].upper()
    instance_id = sys.argv[2]

    ec2 = boto3.client('ec2')

    if action == 'START':
        # DryRun to check permission
        try:
            ec2.start_instances(
                InstanceIds=[
                    instance_id
                ],
                DryRun=True
            )
        except ClientError as e:
            if 'InvalidInstanceID' in str(e):
                print('Please enter valid instance id..')
                exit(1)
            elif 'DryRunOperation' not in str(e):
                print('Check with your admin for ec2 permissions')

        # DryRun succeed, now start the instance    
        try:
            response = ec2.start_instances(InstanceIds=[instance_id],DryRun=False)
            print(response)
        except ClientError as e:
            print(e)
    elif action == 'STOP':
        #DryRun to check permission
        try:
            ec2.stop_instances(
                InstanceIds=[
                    instance_id
                ],
                DryRun=True,
                Force=True
            )
        except ClientError as e:
            if 'InvalidInstanceID' in str(e):
                print('Please enter valid instance id..')
                exit(1)
            elif 'DryRunOperation' not in str(e):
                print('Check with your admin for ec2 permissions')  
        
        # DryRun succeed, now stop the instance
        try:
            response = ec2.stop_instances(InstanceIds=[instance_id],DryRun=False)
            print(response)
        except ClientError as e:
            print(e)
    else:
        pass
except IndexError:
    print('Please enter the arguments while you execute the program. Example: \'ec2_start_stop start instance_id\'')