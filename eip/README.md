### We have a scenario where EIP of the first instance should get attached to 2nd instance when 1st instance get terminated in a AutoScaling Group.

## Steps:

#### 1. Configure the Lifecycle Hook in the AutoScaling Group
![lifecycle](images/lifecycle.png)

### 2. Create a cloudwatch which will trigger a SNS topic
![CloudWatch](images/cloudwatch_rule_lifecycle.PNG)

### 3. Configure the Lambda function which will execute eip.py as main function and also attach a role which having permission to access AutoScaling instances.

#### As of now, executing with a Role which having Administrator access. Also make sure than python runtime is 2.7

### 4. In first version of this code, it required a S3 bucket and object which should be available by default. If the bucket and object(eip_allocation_id.txt)will be unavailable then it is going to generate error.