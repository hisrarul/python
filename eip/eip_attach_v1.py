from __future__ import print_function
import boto3
from botocore.exceptions import ClientError
import logging
import json

ec2 = boto3.client('ec2')
s3 = boto3.resource('s3')

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logging.basicConfig()

def main(event,context):
    logging.info(event)

    # Fetching the instance id using json to avoid the error
    # TypeError: string indices must be integers
    instance_id = event['Records'][0]['Sns']['Message']
    instance_id = json.loads(instance_id)
    instance_id = instance_id['detail']['EC2InstanceId']

    print('Launched Instance Id: ' + instance_id)
    # get_pub_ip = ec2.describe_instances(InstanceIds=[instance_id])
    try:
        s3.meta.client.download_file('demodemodemoashutosh', 'eip_allocation_id.txt', 'alloc.txt')
        with open("alloc.txt", 'r') as f:
            alloc_id = f.read()
            f.close()
        logging.info('Allocation Id: ' + alloc_id)
        allocation_status = ec2.describe_addresses( AllocationIds=[alloc_id])
        if 'AssociationId' in allocation_status['Addresses'][0]:
            logging.info(alloc_id + ' is associated with ' + allocation_status['Addresses'][0]['InstanceId'])
        else:
            try:
                response = ec2.associate_address(
                                AllocationId=alloc_id,
                                InstanceId=instance_id
                            )
                logging.info('Instance Id: ' + instance_id + '\nAttached to EIP having allocation Id: ' + alloc_id + '\nAssociation Id: ' + response['AssociationId'])
            except ClientError as e:
                logging.info('Invalid Instance Id ' + str(e))
    except FileNotFoundError as e:
        logging.info('/tmp/alloc.txt is not available')
    except ClientError as e:
        if 'InvalidAllocationID' in str(e):
            logging.info('Invalid Allocation Id: ' + alloc_id)
# main({'Records': [{'EventSource': 'aws:sns', 'EventVersion': '1.0', 'EventSubscriptionArn': 'arn:aws:sns:us-east-1:847970100731:AutoScalingGroup:7df28410-a451-4b63-a144-34d52e3d5aca', 'Sns': {'Type': 'Notification', 'MessageId': '6e3beea6-35ac-502a-80e2-bb8a091548fb', 'TopicArn': 'arn:aws:sns:us-east-1:847970100731:AutoScalingGroup', 'Subject': None, 'Message': '{"version":"0","id":"382ea91b-3c53-d67b-86b6-359b1cca8ce7","detail-type":"EC2 Instance Launch Successful","source":"aws.autoscaling","account":"847970100731","time":"2019-04-13T19:37:29Z","region":"us-east-1","resources":["arn:aws:autoscaling:us-east-1:847970100731:autoScalingGroup:97103489-b9cb-4bc2-b69b-6b9ae347b4f6:autoScalingGroupName/asg-demo","arn:aws:ec2:us-east-1:847970100731:instance/i-0081fb6f09200de5a"],"detail":{"Description":"Launching a new EC2 instance: i-0081fb6f09200de5a","Details":{"Subnet ID":"subnet-966976f2","Availability Zone":"us-east-1a"},"EndTime":"2019-04-13T19:37:29.226Z","RequestId":"dfe5a860-cf9b-0102-5cd5-d1ad208492d9","ActivityId":"dfe5a860-cf9b-0102-5cd5-d1ad208492d9","Cause":"At 2019-04-13T19:36:53Z an instance was started in response to a difference between desired and actual capacity, increasing the capacity from 0 to 1.","AutoScalingGroupName":"asg-demo","StartTime":"2019-04-13T19:36:55.744Z","EC2InstanceId":"i-0985d2c3ecf08d8ab","StatusCode":"InProgress","StatusMessage":""}}', 'Timestamp': '2019-04-13T19:37:29.798Z', 'SignatureVersion': '1', 'Signature': 'O/ifE4HDdIbr/oSbu4UH6U/HSBMIna9S3/ztg2zx2wRy2WqyBiBOJ/CjccH1WUnY7cM1xuiK4aGSzqneSX1DyW7Ukag+pddWLSnCxYNJlnW6ufnoQe+H1bV2uv840s3maNGn9cVniFxFZESdcBsbSTDnK9VYjCIsT3OSHQlJ7YnMuco35PCCCJ792VfZl65wcaZIuw4gQj5y1DcVXK0BcdyOQNzYgfl80LNgHuz0LKX03PtotC0nmKOq3UVKEMyahqEGcnqLxshy55hUMSVSVwHKDOaEG7t87y/xQ+2zMvBPCA7bL4ALJm3D9r6ziX0zSUp7qj7fbSqNE3v8Oz2Rzw==', 'SigningCertUrl': 'https://sns.us-east-1.amazonaws.com/SimpleNotificationService-6aad65c2f9911b05cd53efda11f913f9.pem', 'UnsubscribeUrl': 'https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:847970100731:AutoScalingGroup:7df28410-a451-4b63-a144-34d52e3d5aca', 'MessageAttributes': {}}}]},"")
