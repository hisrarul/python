from __future__ import print_function
import boto3
from botocore.exceptions import ClientError
import logging
import json

ec2 = boto3.client('ec2')
s3 = boto3.resource('s3')

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logging.basicConfig()

def main(event,context):
    logging.info(event)

    # Fetching the instance id using json to avoid the error
    # TypeError: string indices must be integers
    instance_id = event['Records'][0]['Sns']['Message']
    instance_id = json.loads(instance_id)
    instance_id = instance_id['detail']['EC2InstanceId']

    print('Instance Id: ' + instance_id)
    get_pub_ip = ec2.describe_instances(InstanceIds=[instance_id])
    try:
        if 'PublicIpAddress' in get_pub_ip['Reservations'][0]['Instances'][0]:
            pub_ip = get_pub_ip['Reservations'][0]['Instances'][0]['PublicIpAddress']
            print('Public IP: ' + pub_ip)
            try:
                # Describe Elastic IP
                check_association = ec2.describe_addresses(PublicIps=[
                        pub_ip,
                    ])
                get_alloc_id = check_association['Addresses'][0]['AllocationId']

                # Save the allocation Id in s3 bucket
                s3.meta.client.download_file('demodemodemoashutosh', 'eip_allocation_id.txt', '/tmp/alloc.txt')
                with open("/tmp/alloc.txt", 'w') as f:
                    f.write(get_alloc_id)
                    f.close()
                s3.meta.client.upload_file('/tmp/alloc.txt', 'demodemodemoashutosh', 'eip_allocation_id.txt')

            except ClientError as e:
                if 'InvalidAddress' in str(e):
                    print('Public IP is not a valid EIP')
        else:
            print('There is no Public IP Address on your instance.' + get_alloc_id)
    except IndexError as e:
        if 'index out of range' in str(e):
            print('Check whether instance id is available')
    logging.info('Sucessfully updated the object with allocation id of the instance')
# main({u'Records': [{u'EventVersion': u'1.0', u'EventSubscriptionArn': u'arn:aws:sns:us-east-1:847970100731:AutoScalingGroup:1b247bd3-93e6-4fb5-a350-5bb8b44ef60f', u'EventSource': u'aws:sns', u'Sns': {u'SignatureVersion': u'1', u'Timestamp': u'2019-04-12T12:48:34.775Z', u'Signature': u'ieZNCmgeroS2Aclwg62ZxHvr9sgDK0Hi2KzdkAWue3s/3eswPhlsRIms5Puph0X2fyH/anmGRi2PGRXhe4w3GFZn1esxfDY+3dT7S9wjyAfRD/wB2xDHjslhECU1iHCJMnMiH5XIZc+24xWrkzd7bXYUTOfLZvgChxXV6ziA+kIAzbAfF0hdTzLvF463dQY23oGrlCzcIkbROQsUyPbQG54YCgcceu/FBTRwQOPDSk63O+RE0gMlrzegOMvb65huofHaGDCV06zo82FtCRXJKjLZABtQ+JgRbt7Foh+9EfRFkywkAaX+PlSf2Ks0aaB9yEG9McmaLRXcCfC6NE5opg==', u'SigningCertUrl': u'https://sns.us-east-1.amazonaws.com/SimpleNotificationService-6aad65c2f9911b05cd53efda11f913f9.pem', u'MessageId': u'0f0b1285-9479-5faf-8b1d-47af1f1b5600', u'Message': u'{"version":"0","id":"0b244403-5080-8b19-4358-c2a17603e734","detail-type":"EC2 Instance-terminate Lifecycle Action","source":"aws.autoscaling","account":"847970100731","time":"2019-04-12T12:48:34Z","region":"us-east-1","resources":["arn:aws:autoscaling:us-east-1:847970100731:autoScalingGroup:97103489-b9cb-4bc2-b69b-6b9ae347b4f6:autoScalingGroupName/asg-demo"],"detail":{"LifecycleActionToken":"76f03821-54cf-4191-a735-a47790307f8f","AutoScalingGroupName":"asg-demo","LifecycleHookName":"demo","EC2InstanceId":"i-0478290395ce705ac","LifecycleTransition":"autoscaling:EC2_INSTANCE_TERMINATING"}}', u'MessageAttributes': {}, u'Type': u'Notification', u'UnsubscribeUrl': u'https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:847970100731:AutoScalingGroup:1b247bd3-93e6-4fb5-a350-5bb8b44ef60f', u'TopicArn': u'arn:aws:sns:us-east-1:847970100731:AutoScalingGroup', u'Subject': None}}]},"")