ec2 = boto3.client('ec2')

# Describe Elastic IP
response = ec2.describe_addresses(PublicIps=[
        '52.1.17.124',
    ])

# Check EIP associated with Instance
if 'AssociationId' not in response['Addresses'][0]:
    print('Is not associated')
else:
    print('EIP associated with ' + response['Addresses'][0]['InstanceId'])