import boto3

ec2 = boto3.client('ec2')
get_pub_ip = ec2.describe_instances(InstanceIds=['i-01d131702fb2562f6'])
if 'PublicIpAddress' in get_pub_ip:
    pub_ip = get_pub_ip['Reservations'][0]['Instances'][0]['PublicIpAddress']

    # Describe Elastic IP
    check_association = ec2.describe_addresses(PublicIps=[
            pub_ip,
        ])

    # Check EIP associated with Instance
    if 'AssociationId' not in check_association['Addresses'][0]:
        print(pub_ip + ' Is not associated')
    else:
        print(pub_ip + ' is associated with Instance Id ' + check_association['Addresses'][0]['InstanceId'])
else:
    print('There is no Public IP Address on your instance.')