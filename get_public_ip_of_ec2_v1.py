import boto3

# Get the Public IP address 
ec2 = boto3.resource('ec2')
instance = ec2.Instance('i-0c3fbe9607bc563f5')
pub_ip = instance.public_ip_address

if pub_ip is None:
    print('No Public IP assigned')
else:
    print(pub_ip)