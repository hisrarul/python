import boto3
import sys

account_id = '817338342595'
def credentials(account_id):
  sts_client = boto3.client('sts')
  assumed_role_object=sts_client.assume_role(
    RoleArn='arn:aws:iam::' + str(account_id) + ':role/CrossAccountFromIsrar',
    RoleSessionName='RoleSession1'
  )
  credentials=assumed_role_object['Credentials']
  temp_access = (credentials['AccessKeyId'], credentials['SecretAccessKey'], credentials['SessionToken'])
  return temp_access

def main():
  credential = credentials(account_id)
  sts_client = boto3.client('sts')
  iam = boto3.client('iam',
          aws_access_key_id=credential[0],
          aws_secret_access_key=credential[1],
          aws_session_token=credential[2]
          )
  for user in sys.argv[1:]:
    response = iam.create_user(
        Path='/',
        UserName=user,
    )
  return(response)
main()