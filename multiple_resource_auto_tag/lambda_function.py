from __future__ import print_function
import boto3
import json
import logging
import time
import datetime

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def credentials(account_id):
  sts_client = boto3.client('sts')
  assumed_role_object=sts_client.assume_role(
    RoleArn='arn:aws:iam::' + str(account_id) + ':role/CrossAccountFromIsrar',
    RoleSessionName='RoleSession1'
  )
  credentials=assumed_role_object['Credentials']
  temp_access = (credentials['AccessKeyId'], credentials['SecretAccessKey'], credentials['SessionToken'])
  return temp_access

def costcenter(account_id):
  credential = credentials(account_id)
  organization = boto3.client('organizations',
                              aws_access_key_id= credential[0],
                              aws_secret_access_key = credential[1],
                              aws_session_token = credential[2])

  response = organization.list_tags_for_resource(
      ResourceId=account_id)
  costcenter_val = (response['Tags'][0]['Value'])
  return costcenter_val

def lambda_handler(event, context):
    logger.info(event)
    region = event['region']
    detail = event['detail']
    eventname = detail['eventName']
    account_id = event['account']
    creator_arn = event['detail']['userIdentity']['arn']
    logger.info(account_id)

#Calling credentials function
    credential = credentials(account_id)
    mytags = [
          {
              "Key" : "_Owner",
              "Value" : creator_arn
          },
          {
              "Key" : "_Account",
              "Value" : account_id
          },
          {
              "Key" : "_CostCenter",
              "Value" : costcenter(account_id)
          }
      ]

#Create tag for ec2 instance
    if eventname == 'RunInstances':
        ec2 = boto3.resource('ec2',
          aws_access_key_id=credential[0],
          aws_secret_access_key=credential[1],
          aws_session_token=credential[2],
        )
        instance_id = event['detail']['responseElements']['instancesSet']['items'][0]['instanceId']
        print(instance_id)
        ec2.create_tags(
                    Resources = [instance_id],
                    Tags= mytags
                )

#Create tag for s3 buckets
    elif eventname == 'CreateBucket':
        s3 = boto3.client("s3",
          aws_access_key_id=credential[0],
          aws_secret_access_key=credential[1],
          aws_session_token=credential[2],
        )
        bucket_name = detail['requestParameters']['bucketName']
        s3.put_bucket_tagging(Bucket=bucket_name, Tagging={'TagSet': mytags})

#Create tags on DB Instance
    elif eventname == 'CreateDBInstance':
        rds = boto3.client('rds',
          aws_access_key_id=credential[0],
          aws_secret_access_key=credential[1],
          aws_session_token=credential[2],
        )
        resource_arn = detail['responseElements']['dBInstanceArn']
        rds.add_tags_to_resource(ResourceName=resource_arn, Tags=mytags)

#Create tags on VPC
    elif eventname == 'CreateVpc':
        vpc_id = detail['responseElements']['vpc']['vpcId']

        ec2 = boto3.resource('ec2',
          aws_access_key_id=credential[0],
          aws_secret_access_key=credential[1],
          aws_session_token=credential[2],
        )
        vpc = ec2.Vpc(vpc_id)
        vpc.create_tags(DryRun=False, Tags=mytags)

#Create tags on Redshift Cluster
    elif eventname == 'CreateCluster':
        cluster_name = detail['requestParameters']['clusterIdentifier']
        client = boto3.client('redshift',
          aws_access_key_id=credential[0],
          aws_secret_access_key=credential[1],
          aws_session_token=credential[2]
        )
        resource_arn = "arn:aws:redshift:" + str(region) + ":" + str(account_id) + ":cluster:" + cluster_name
        logger.info("RedShift arn: " + resource_arn)
        client.create_tags(ResourceName=resource_arn, Tags=mytags)

#Create tags on dynamodb table
    elif eventname == 'CreateTable':
        client = boto3.client('dynamodb',
          aws_access_key_id=credential[0],
          aws_secret_access_key=credential[1],
          aws_session_token=credential[2]
        )
        resource_arn = detail['responseElements']['tableDescription']['tableArn']
        client.tag_resource(ResourceArn=resource_arn, Tags=mytags)

#Create tags on EBS volume
    elif eventname == 'CreateVolume':
        volume_id = detail['responseElements']['volumeId']
        print (volume_id)
        ec2 = boto3.resource('ec2',
          aws_access_key_id=credential[0],
          aws_secret_access_key=credential[1],
          aws_session_token=credential[2]
        )
        vol = ec2.Volume(volume_id)
        vol.create_tags(DryRun=False,Tags=mytags)

#lambda_handler({'version': '0', 'id': '084ae684-5d97-7225-7b16-52cc9e143894', 'detail-type': 'AWS API Call via CloudTrail', 'source': 'aws.ec2', 'account': '817338342595', 'time': '2019-08-07T02:33:05Z', 'region': 'us-east-1', 'resources': [], 'detail': {'eventVersion': '1.05', 'userIdentity': {'type': 'Root', 'principalId': '817338342595', 'arn': 'arn:aws:iam::817338342595:root', 'accountId': '817338342595', 'accessKeyId': 'ASIA34TJJLTBYBHJRB77', 'userName': 'ashok315', 'sessionContext': {'attributes': {'mfaAuthenticated': 'false', 'creationDate': '2019-08-07T01:29:24Z'}}, 'invokedBy': 'signin.amazonaws.com'}, 'eventTime': '2019-08-07T02:33:05Z', 'eventSource': 'ec2.amazonaws.com', 'eventName': 'RunInstances', 'awsRegion': 'us-east-1', 'sourceIPAddress': '75.33.242.80', 'userAgent': 'signin.amazonaws.com', 'requestParameters': {'instancesSet': {'items': [{'imageId': 'ami-035b3c7efe6d061d5', 'minCount': 1, 'maxCount': 1, 'keyName': 'autoscaling'}]}, 'groupSet': {'items': [{'groupId': 'sg-094f3e7e9b3fa7841'}]}, 'instanceType': 't2.micro', 'blockDeviceMapping': {'items': [{'deviceName': '/dev/xvda', 'ebs': {'volumeSize': 8, 'deleteOnTermination': True, 'volumeType': 'gp2'}}]}, 'monitoring': {'enabled': False}, 'disableApiTermination': False, 'ebsOptimized': False, 'creditSpecification': {'cpuCredits': 'standard'}}, 'responseElements': {'requestId': '25472b93-3805-4b7d-9835-8042d5fb6587', 'reservationId': 'r-077b0dc5b97a988c2', 'ownerId': '817338342595', 'groupSet': {}, 'instancesSet': {'items': [{'instanceId': 'i-09d2c8adce2b4a459', 'imageId': 'ami-035b3c7efe6d061d5', 'instanceState': {'code': 0, 'name': 'pending'}, 'privateDnsName': 'ip-172-31-90-254.ec2.internal', 'keyName': 'autoscaling', 'amiLaunchIndex': 0, 'productCodes': {}, 'instanceType': 't2.micro', 'launchTime': 1565145184000, 'placement': {'availabilityZone': 'us-east-1c', 'tenancy': 'default'}, 'monitoring': {'state': 'disabled'}, 'subnetId': 'subnet-0b43102c95508605d', 'vpcId': 'vpc-04434f7f23c27a607', 'privateIpAddress': '172.31.90.254', 'stateReason': {'code': 'pending', 'message': 'pending'}, 'architecture': 'x86_64', 'rootDeviceType': 'ebs', 'rootDeviceName': '/dev/xvda', 'blockDeviceMapping': {}, 'virtualizationType': 'hvm', 'hypervisor': 'xen', 'groupSet': {'items': [{'groupId': 'sg-094f3e7e9b3fa7841', 'groupName': 'launch-wizard-128'}]}, 'sourceDestCheck': True, 'networkInterfaceSet': {'items': [{'networkInterfaceId': 'eni-0b826646d4f046d79', 'subnetId': 'subnet-0b43102c95508605d', 'vpcId': 'vpc-04434f7f23c27a607', 'ownerId': '817338342595', 'status': 'in-use', 'macAddress': '12:2a:d5:34:b3:3e', 'privateIpAddress': '172.31.90.254', 'privateDnsName': 'ip-172-31-90-254.ec2.internal', 'sourceDestCheck': True, 'interfaceType': 'interface', 'groupSet': {'items': [{'groupId': 'sg-094f3e7e9b3fa7841', 'groupName': 'launch-wizard-128'}]}, 'attachment': {'attachmentId': 'eni-attach-0a94421ea80081937', 'deviceIndex': 0, 'status': 'attaching', 'attachTime': 1565145184000, 'deleteOnTermination': True}, 'privateIpAddressesSet': {'item': [{'privateIpAddress': '172.31.90.254', 'privateDnsName': 'ip-172-31-90-254.ec2.internal', 'primary': True}]}, 'ipv6AddressesSet': {}, 'tagSet': {}}]}, 'ebsOptimized': False, 'cpuOptions': {'coreCount': 1, 'threadsPerCore': 1}, 'capacityReservationSpecification': {'capacityReservationPreference': 'open'}}]}}, 'requestID': '25472b93-3805-4b7d-9835-8042d5fb6587', 'eventID': '103d1196-60e9-409a-b41a-da9ff88ec2f4', 'eventType': 'AwsApiCall'}},'')