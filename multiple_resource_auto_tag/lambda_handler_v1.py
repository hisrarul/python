from __future__ import print_function
import boto3
import json
from botocore.exceptions import ClientError
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def lambda_handler(event, context):
  
    logger.info(event)
    sts_client = boto3.client('sts')
    account_id = event['account']
    creator_arn = event['detail']['userIdentity']['arn']

    logger.info('Account Id ' + account_id)

    assumed_role_object=sts_client.assume_role(
       RoleArn='arn:aws:iam::' + str(account_id) + ':role/CrossAccountFromIsrar',
       RoleSessionName='RoleSession1'
    )
    #print logs in cloudwatch
    logging.info('Assume Role:::' + assumed_role_object['AssumedRoleUser']['Arn'])

    #Get the temporary credentials
    credentials=assumed_role_object['Credentials']  
    mytags = [
        {
           "Key" : "Creator",
           "Value" : creator_arn
        },
        {
           "Key" : "Account", 
           "Value" : account_id
        }]  
    if (event['detail']['eventName'] == 'RunInstances'):
        ec2 = boto3.resource('ec2',
        aws_access_key_id=credentials['AccessKeyId'],
        aws_secret_access_key=credentials['SecretAccessKey'],
        aws_session_token=credentials['SessionToken'],
        )
        instance_id = event['detail']['responseElements']['instancesSet']['items'][0]['instanceId']
        try:
            response = ec2.create_tags(
                       Resources = [instance_id],
                       Tags= mytags
                   )
            logger.info('Result:::' + str(response))
        except ClientError as e:
            if 'InvalidInstanceID' or 'InvalidID' in str(e):
                logging.info('Instance Id:::' + instance_id + ':::Is invalid')
            else:
                print(str(e))
#lambda_handler({u'account': u'817338342595', u'region': u'us-east-1', u'detail': {u'eventVersion': u'1.05', u'eventID': u'a07b41e6-25a6-4be9-a6c0-95d5e4a3c434', u'eventTime': u'2019-07-05T04:43:31Z', u'requestParameters': {u'groupSet': {u'items': [{u'groupId': u'sg-058a18c790787940f'}]}, u'monitoring': {u'enabled': False}, u'capacityReservationSpecification': {u'capacityReservationPreference': u'open'}, u'instanceInitiatedShutdownBehavior': u'stop', u'tenancy': u'default', u'creditSpecification': {u'cpuCredits': u'standard'}, u'blockDeviceMapping': {u'items': [{u'deviceName': u'/dev/xvda', u'ebs': {u'deleteOnTermination': True, u'volumeSize': 8, u'volumeType': u'gp2'}}]}, u'instancesSet': {u'items': [{u'minCount': 1, u'maxCount': 1, u'keyName': u'autoscaling', u'imageId': u'ami-0b898040803850657'}]}, u'disableApiTermination': False, u'instanceType': u't2.micro', u'ebsOptimized': False}, u'eventType': u'AwsApiCall', u'responseElements': {u'ownerId': u'817338342595', u'groupSet': {}, u'reservationId': u'r-0ae076a70eb89c3c4', u'requestId': u'54ede52b-ff74-4dbe-a25d-8616b2d47fc3', u'instancesSet': {u'items': [{u'productCodes': {}, u'vpcId': u'vpc-04434f7f23c27a607', u'cpuOptions': {u'threadsPerCore': 1, u'coreCount': 1}, u'instanceId': u'i-0648e961639ca6e', u'imageId': u'ami-0b898040803850657', u'keyName': u'autoscaling', u'subnetId': u'subnet-0b43102c95508605d', u'amiLaunchIndex': 0, u'instanceType': u't2.micro', u'groupSet': {u'items': [{u'groupName': u'launch-wizard-43', u'groupId': u'sg-058a18c790787940f'}]}, u'monitoring': {u'state': u'disabled'}, u'capacityReservationSpecification': {u'capacityReservationPreference': u'open'}, u'stateReason': {u'message': u'pending', u'code': u'pending'}, u'privateIpAddress': u'172.31.82.193', u'virtualizationType': u'hvm', u'privateDnsName': u'ip-172-31-82-193.ec2.internal', u'sourceDestCheck': True, u'blockDeviceMapping': {}, u'placement': {u'tenancy': u'default', u'availabilityZone': u'us-east-1c'}, u'hypervisor': u'xen', u'networkInterfaceSet': {u'items': [{u'status': u'in-use', u'macAddress': u'12:b2:cb:06:d5:44', u'sourceDestCheck': True, u'vpcId': u'vpc-04434f7f23c27a607', u'ipv6AddressesSet': {}, u'networkInterfaceId': u'eni-0857a973e4b381989', u'privateDnsName': u'ip-172-31-82-193.ec2.internal', u'groupSet': {u'items': [{u'groupName': u'launch-wizard-43', u'groupId': u'sg-058a18c790787940f'}]}, u'interfaceType': u'interface', u'attachment': {u'status': u'attaching', u'deviceIndex': 0, u'deleteOnTermination': True, u'attachmentId': u'eni-attach-0b3651adf9fe44ce1', u'attachTime': 1562301810000}, u'tagSet': {}, u'subnetId': u'subnet-0b43102c95508605d', u'ownerId': u'817338342595', u'privateIpAddressesSet': {u'item': [{u'privateDnsName': u'ip-172-31-82-193.ec2.internal', u'primary': True, u'privateIpAddress': u'172.31.82.193'}]}, u'privateIpAddress': u'172.31.82.193'}]}, u'ebsOptimized': False, u'launchTime': 1562301810000, u'architecture': u'x86_64', u'instanceState': {u'code': 0, u'name': u'pending'}, u'rootDeviceType': u'ebs', u'rootDeviceName': u'/dev/xvda'}]}}, u'awsRegion': u'us-east-1', u'eventName': u'RunInstances', u'userIdentity': {u'userName': u'israr', u'principalId': u'AIDA34TJJLTBQJ5XSKWFX', u'accessKeyId': u'ASIA34TJJLTBXAEZXOWB', u'invokedBy': u'signin.amazonaws.com', u'sessionContext': {u'attributes': {u'creationDate': u'2019-07-05T02:55:08Z', u'mfaAuthenticated': u'false'}}, u'type': u'IAMUser', u'arn': u'arn:aws:iam::817338342595:user/israr', u'accountId': u'817338342595'}, u'eventSource': u'ec2.amazonaws.com', u'requestID': u'54ede52b-ff74-4dbe-a25d-8616b2d47fc3', u'userAgent': u'signin.amazonaws.com', u'sourceIPAddress': u'103.88.217.233'}, u'detail-type': u'AWS API Call via CloudTrail', u'source': u'aws.ec2', u'version': u'0', u'time': u'2019-07-05T04:43:31Z', u'id': u'9f792d1b-9aa3-2bd1-4c67-8224abcb420f', u'resources': []},'')