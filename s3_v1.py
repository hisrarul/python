import boto3
import datetime

d = datetime.datetime.now()
date=d.strftime("%Y%d%m%H%M")

bucket = input("Enter a keyword to prefix with bucket name: ")
if bucket is '':
    bucket = 'israrulhaque'

bucketname = bucket + str(date)


#Create a client
client = boto3.client('s3')

response = client.create_bucket(
    ACL='private',
    Bucket=bucketname,
    CreateBucketConfiguration={
        'LocationConstraint':'us-east-2'
    }
)

# response = {'ResponseMetadata': {'RequestId': '24930A184E6E3A28', 'HostId': 'Qgw6EVnSuRCNh+usyf4bo6n7tBjrrvhHlEIUDVR5DG2R5045e/HDzs5zqc3w3jJRv+982j5ESqQ=', 'HTTPStatusCode': 200, 'HTTPHeaders': {'x-amz-id-2': 'Qgw6EVnSuRCNh+usyf4bo6n7tBjrrvhHlEIUDVR5DG2R5045e/HDzs5zqc3w3jJRv+982j5ESqQ=', 'x-amz-request-id': '24930A184E6E3A28', 'date': 'Tue, 19 Mar 2019 16:22:27 GMT', 'location': 'http://haquemanzil201919032152.s3.amazonaws.com/', 'content-length': '0', 'server': 'AmazonS3'}, 'RetryAttempts': 0}, 'Location': 'http://haquemanzil201919032152.s3.amazonaws.com/'}
print("Bucket URL: " + response["ResponseMetadata"]["HTTPHeaders"]["location"])