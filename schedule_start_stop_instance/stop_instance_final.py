import boto3
import logging
from botocore.exceptions import ClientError

logger = logging.getLogger()
logger.setLevel(logging.INFO)

ec2client = boto3.client('ec2')
asclient = boto3.client('autoscaling')

def stopinstance(instanceId):
    ec2client.stop_instances(
        InstanceIds=[
            instanceId
        ],
        Force=True)
    return ('Successfully stop ' + instanceId)


def lambda_handler(event,context):
    instanceData = ec2client.describe_instances(
        Filters=[
            {
                'Name': 'tag:ApplicationRole',
                'Values': [
                    'jumphost',
                ]
            },
        ])
    for reservation in instanceData["Reservations"]:
        for instance in reservation["Instances"]:
            instanceId = instance["InstanceId"]
            logger.info('##AutoScaling group')
            asData = asclient.describe_auto_scaling_instances(
            InstanceIds=[
                instanceId
                ]
            )
            logger.info(asData)
            logger.info('Stopping Instance, please wait.. ' + instanceId)
            # Get the ASG name
            try:
                asName = asData['AutoScalingInstances'][0]['AutoScalingGroupName']
                if asName:
                    response = asclient.suspend_processes(
                            AutoScalingGroupName=asName,
                            ScalingProcesses=[
                                'Terminate'
                            ]
                        )
                    logger.info('Suspend Process' + str(response))
                    try:
                        stopinstance(instanceId)
                    except ClientError as e:
                        if 'IncorrectInstanceState' in str(e):
                            logger.info('Already Terminated ' + instanceId)
            except:
                try:
                    stopinstance(instanceId)
                except ClientError as e:
                    if 'IncorrectInstanceState' in str(e):
                        logger.info('Already Terminated ' + instanceId)
    return('Success!')
