import boto3
import logging
import datetime
from botocore.exceptions import ClientError

d = datetime.datetime.now()
date=d.strftime("%Y%d%m%H%M")
ami_name = 'instance_backup_' + str(date)

logger = logging.getLogger()
logger.setLevel(logging.INFO)
logging.basicConfig()


ec2 = boto3.client('ec2')

# Send log to cloudwatch
def my_logging_handler(event, context):
    logger.info('got event{}'.format(event))
    # logger.error('something went wrong')
    # return 'Check Logs'
    
def main(event,context):
    try:
        my_logging_handler(event,context)
        instance_id = event['detail']['EC2InstanceId']
        response = ec2.create_image(InstanceId=instance_id, Name=ami_name)
        logging.info('Snapshot has been created')
        print('AMI has been created: '+ response['ImageId'])
    except ClientError as e:
        if 'InvalidInstanceID' in str(e):
            print('Please enter valid instance id..')
            exit(1)
