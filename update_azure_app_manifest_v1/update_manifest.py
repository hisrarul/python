import json


# Read the content of update.json file
with open('update.json', 'r') as updated_role:
    new_role = json.load(updated_role)

# Read the content of manifest.json file which has list
with open('manifest.json', 'r') as manifest:
    fest_data = json.load(manifest)

# Append the list with update.json
fest_data.append(new_role)

# Write the updated list in the manifest.json file
with open('manifest.json', 'w') as updated_manifest:
    new_fest_data = json.dump(fest_data, updated_manifest)