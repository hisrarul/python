import json
import csv
import subprocess

update = {
         "allowedMemberTypes": [
         "User"
          ],
         "description": "AWS-ADMINS",
         "displayName": "AWS ADMINS Account 4",
         "isEnabled": "true",
         "value": "arn:aws:iam::132132132132:role/AWS-ADMINS,arn:aws:iam::132132132132:saml-provider/Israrul"
        }

full_cmd = "az ad app show --id ec825aa7-b145-4637-a13b-e22f6b9ac3d3 --query appRoles"
with open("manifest.json", 'w') as f:
    p = subprocess.Popen(full_cmd, shell=True, stdout=f)
    p.communicate()

# Open the CSV
f = open('newRolesDetails.csv', 'r')
# reader = csv.DictReader(f, fieldnames=("fieldname0", "fieldname1", "fieldname2", "fieldname3", "fieldname4"))
reader = csv.DictReader(f)
# Parse the CSV into JSON
out = json.dumps([row for row in reader])
print("JSON parsed!")
# Save the JSON
f = open('update.json', 'w')
f.write(out)
print("JSON saved!")

with open("update.json") as f:
    info_in_csv = json.load(f)

for info in info_in_csv:
    update['description'] = info['description']
    update['displayName'] = info['displayName']
    update['value'] = info['value']
    print(update)

    with open('manifest.json', 'r') as manifest:
        fest_data = json.load(manifest)

    fest_data.append(update)

    with open('manifest.json', 'w') as updated_manifest:
        new_fest_data = json.dump(fest_data, updated_manifest)
