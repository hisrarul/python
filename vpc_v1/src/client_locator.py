# We are creating this class to locate our resource with ease by hiding boto3 region and resource like ec2, vpc etc
# This way we can avoid calling the client again and again.
import boto3


class ClientLocator:
    # Define our initialize method
    # we are going to pass in the client we are going to use like vpc, ec2
    # This clientLocator class will get me the client for that class like ec2, vpc etc
    def __init__(self, client):     # Allow to pass client parameter
        self._client = boto3.client(client, region_name="us-east-1")    # Will have a Local client definition

    def get_client(self):   # Creating another method
        return self._client


class EC2Client(ClientLocator):
    # Another class that is going to define. It is going to get a "ClientLocator" parameter
    def __init__(self):     # Initialize class itself with boto3 parameter
        super().__init__('ec2')            # Call the super method so I can get the client locator both of the implementation & going to initialize with the client that I want to use.
