class VPC:
    def __init__(self, client):     # Initialize Class
        self._client = client
        """ :type : pyboto3.ec2"""      # To enable the auto fill suggestions using "pyboto3"

    def create_vpc(self):       # Create a method to create a VPC
        print('Creating a VPC...')
        return self._client.create_vpc(
            CidrBlock='40.0.0.0/16'
        )

    def add_name_tag(self, resource_id, resource_name):     # Create a method to tag every resource
        print('Adding ' + resource_name + ' tag to the ' + resource_id)
        return self._client.create_tags(
            Resources=[resource_id],
            Tags=[{
                'Key': 'Name',
                'Value': resource_name
            }]
        )
